﻿using IOF.XML.V3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ONZ.PointsCalculator {
  public class IOFXMLResultLoader : IResultLoader {
    private ResultList LoadFromXml(byte[] rawFile) {
      Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
      string utf8String = Encoding.UTF8.GetString(Encoding.Convert(Encoding.GetEncoding(1252), Encoding.UTF8, rawFile));

      using (var stream = new StringReader(utf8String)) {
        using (var reader = XmlReader.Create(stream)) {
          return (ResultList)new XmlSerializer(typeof(ResultList)).Deserialize(reader);
        }
      }
    }

    public List<ResultClass> Load(byte[] rawFile) {
      ResultList xml = LoadFromXml(rawFile);
      return ConvertFromXml(xml);
    }

    public List<ResultClass> ConvertFromXml(ResultList xml) {
      Func<ClassResult, string, List<ResultItem>> individualPlacings = (p, grade) => (p.PersonResult ?? new PersonResult[0]).Select(r => new ResultItem {
        Grade = grade,
        Club = r.Organisation?.Name ?? "",
        Name = ((r.Person?.Name?.Given ?? "") + " " + (r.Person?.Name?.Family ?? "")).Trim(),
        Place = GetInt(r.Result?[0]?.Position),
        Time = r.Result?[0]?.TimeSpecified ?? false ? (decimal?)r.Result?[0]?.Time : (decimal?)null,
        Controls = r.Result?[0]?.SplitTime == null ? new List<int>() : r.Result?[0]?.SplitTime.Where(c => c.TimeSpecified).Select(c => GetInt(c.ControlCode) ?? 0).ToList()
      }).ToList();

      Func<ClassResult, string, List<ResultItem>> teamPlacings = (p, grade) => (p.TeamResult ?? new TeamResult[0]).Select(t => {
        var tr = (t.TeamMemberResult.LastOrDefault() ?? new TeamMemberResult { })?.Result?[0]?.OverallResult;
        var time = tr.TimeBehindSpecified ? (decimal?)tr.TimeBehind : (decimal?)null;
        var place = int.TryParse(tr.Position, out int pos) ? pos : (int?)null;

        return new ResultItem {
          Grade = grade,
          Club = t.Organisation?[0]?.Name ?? "",
          Name = t.Name,
          Time = time,
          Place = place,
          Controls = new List<int>()
        };
      }).ToList();

      return xml.ClassResult.Select(p => {
        var ip = individualPlacings(p, p.Class.Name);
        var tp = teamPlacings(p, p.Class.Name);

        return new ResultClass {
          Name = p.Class.Name,
          Placings = ip != null && ip.Count > 0 ? ip : tp
        };
      }).ToList();
    }

    private int? GetInt(string input) {
      if (String.IsNullOrWhiteSpace(input)) {
        return null;
      }

      if (Int32.TryParse(input, out int result)) {
        return result;
      }

      return null;
    }
  }
}