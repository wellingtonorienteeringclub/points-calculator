# Orienteering points calculator

# Usage
Browse to ``https://points-calculator.orienteeringwellington.nz/``. This website can be used offline so as long as you have browsed to the site at some point you should be able to access it without any internet connection. In some browser (Chrome on Windows) you will see an install button in the toolbar. IF you install the app it will be available from the Windows start menu.

Sample files for different events are available at the bitbucket ``https://bitbucket.org/wellingtonorienteeringclub/points-calculator/downloads/``.

# Configuration
To be done...