using System.Threading.Tasks;
using Blazor.FileReader;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using BlazorFileSaver;

namespace ONZ.PointsCalculator.Web {
  public class Program {
    public static async Task Main(string[] args) {
      var builder = WebAssemblyHostBuilder.CreateDefault(args);
      builder.RootComponents.Add<App>("app");

      builder.Services.AddBaseAddressHttpClient();
      builder.Services.AddFileReaderService(options => options.UseWasmSharedBuffer = true);
      builder.Services.AddScoped<IOFXMLResultLoader, IOFXMLResultLoader>();
      builder.Services.AddScoped<IBlazorFileSaver, BlazorFileSaverJS>();
      builder.Services.AddScoped<IJsonSerializerService, JsonSerializerService>();
      builder.Services.AddScoped<FileWriter, FileWriter>();
      builder.Services.AddScoped<BufferLogger, BufferLogger>();
      builder.Services.AddBlazorFileSaver();

      await builder.Build().RunAsync();
    }
  }
}
