﻿using ONZ.PointsCalculator;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ONZ.PointsCalculator.Web {
  internal class ResultFileLoader : IResultFileLoader {
    private List<UploadedFile> _Files;
    private readonly ILogger _Logger;

    public ResultFileLoader(List<UploadedFile> files, ILogger logger) {
      _Files = files;
      _Logger = logger;
    }

    public byte[] Load(string filename) {
      _Logger.Log($"Looking for file {filename}");

      var file = _Files.FirstOrDefault(p => p.Name == filename);

      if (file != null) {
        return file.RawFile;
      }

      _Logger.Log($"Failed to find {filename}, has {String.Join(",", _Files.Select(p=>p.Name))}");

      return null;
    }
  }
}