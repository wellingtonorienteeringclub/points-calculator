﻿using ONZ.PointsCalculator;
using System.Text.Json;

namespace ONZ.PointsCalculator.Web {
  internal class JsonSerializerService : IJsonSerializerService {
    public T Deserialize<T>(string json) {
      return JsonSerializer.Deserialize<T>(json, GetOptions());
    }

    private JsonSerializerOptions GetOptions() {
      return new JsonSerializerOptions { WriteIndented = true, PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
    }

    public string Serialize<T>(T instance) {
      return JsonSerializer.Serialize(instance, GetOptions());
    }
  }
}