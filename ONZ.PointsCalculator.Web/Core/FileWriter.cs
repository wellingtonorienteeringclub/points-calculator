﻿using ONZ.PointsCalculator;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace ONZ.PointsCalculator.Web {
  public class FileWriter : IResultFileProducer {
    private readonly IJsonSerializerService _JsonSerializerService;

    private List<ArchiveFile> _FileList;

    public byte[] Result { get; private set; }

    public FileWriter(IJsonSerializerService jsonSerializerService) {
      _JsonSerializerService = jsonSerializerService;
    }

    public void End() {
      using (var packageStream = new MemoryStream()) {
        using (var archive = new ZipArchive(packageStream, ZipArchiveMode.Create, true)) {
          foreach (var virtualFile in _FileList) {
            //Create a zip entry for each attachment
            var zipFile = archive.CreateEntry(virtualFile.Name);
            using (var sourceFileStream = new MemoryStream(virtualFile.FileBytes))
            using (var zipEntryStream = zipFile.Open()) {
              sourceFileStream.CopyTo(zipEntryStream);
            }
          }
        }
        Result = packageStream.ToArray();
      }
    }

    public void Init() {
      _FileList = new List<ArchiveFile>();
    }

    public void Write(string folder, string fileName, string message) {
      Add(folder, fileName, Encoding.UTF8.GetBytes(message));
    }

    public void WriteInstance<T>(string folder, string fileName, T instance) {
      Add(folder, fileName, Encoding.UTF8.GetBytes(_JsonSerializerService.Serialize(instance)));
    }

    private void Add(string folder, string fileName, byte[] rawBytes) {
      string path = Path.Combine(folder, fileName.Replace("/", "").Replace(" ", "-"));
      _FileList.Add(new ArchiveFile { FileBytes = rawBytes, Name = path });
    }
  }
}