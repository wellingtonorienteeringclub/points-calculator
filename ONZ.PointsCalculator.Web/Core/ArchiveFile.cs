﻿namespace ONZ.PointsCalculator.Web {
  internal class ArchiveFile {
    public string Name { get;  set; }
    public byte[] FileBytes { get;  set; }
  }
}