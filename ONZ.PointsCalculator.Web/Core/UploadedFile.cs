﻿namespace ONZ.PointsCalculator.Web {
  public class UploadedFile {
    public byte[] RawFile { get; set; }

    public string Name { get; set; }

    public string Type { get;  set; }
  }
}