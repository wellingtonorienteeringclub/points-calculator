﻿using ONZ.PointsCalculator;
using System.Text;

namespace ONZ.PointsCalculator.Web {
  public class BufferLogger : ILogger {
    public StringBuilder Messages { get; private set; } = new StringBuilder();

    public string End() {
      return Messages.ToString();
    }

    public void Log(string message) {
      Messages.AppendLine(message);
    }
  }
}