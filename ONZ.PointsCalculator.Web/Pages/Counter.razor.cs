﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Blazor.FileReader;
using BlazorFileSaver;
using Microsoft.AspNetCore.Components;

namespace ONZ.PointsCalculator.Web {
  public class CounterBase : ComponentBase {
    protected string _Debug = null;
    protected bool Loading = false;
    protected string _Message = "";

    [Inject]
    protected IFileReaderService FileReaderService { get; set; }

    [Inject]
    protected IOFXMLResultLoader IOFXMLResultLoader { get; set; } 

    [Inject]
    protected IBlazorFileSaver BlazorFileSaver { get; set; }

    [Inject]
    protected IJsonSerializerService JsonSerializerService { get; set; }

    [Inject]
    protected FileWriter FileWriter { get; set; }

    [Inject]
    protected BufferLogger BufferLogger { get; set; }

    protected ElementReference InputTypeFileElement;

    public async Task DownloadDebug() {
      await BlazorFileSaver.SaveAs("log.txt", _Debug, "plain/txt");
    }

    public async Task Process() {
      _Message = null;

      var files = new List<UploadedFile>();
      IConfiguration config = null;

      foreach (var file in await FileReaderService.CreateReference(InputTypeFileElement).EnumerateFilesAsync()) {
        await using (Stream stream = await file.OpenReadAsync()) {
          var fi = await file.ReadFileInfoAsync();
          byte[] result = new byte[stream.Length];
          await stream.ReadAsync(result, 0, (int)stream.Length);

          if (fi.Type == "text/xml") {
            files.Add(new UploadedFile { Name = fi.Name, RawFile = result });
          } else if (fi.Type == "application/json") {
            var json = Encoding.UTF8.GetString(result).Trim(new char[] { '\uFEFF', '\u200B' });
            var config3 = JsonSerializerService.Deserialize<Configuration>(json);
            config = ConfigurationInitilizer.Initilize(config3);
          }
        }
      }

      if(config == null) {
        _Message = "Please include a valid configuration file!";
        return;

      }

      Loading = true;
      StateHasChanged();
      await Task.Delay(1000);

      await Task.Run(() => DoProcess(files, config));

      Loading = false;
    }

    private async Task DoProcess(List<UploadedFile> files, IConfiguration config) {
      _Debug = null;
      IResultFileLoader rfl = new ResultFileLoader(files, BufferLogger);

      var results = new Processor(BufferLogger, IOFXMLResultLoader, rfl, config).Process();

      _Debug = BufferLogger.Messages.ToString();

      new ResultFileProducer(FileWriter, config).Produce(results);

      await BlazorFileSaver.SaveAsBase64("results-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip", Convert.ToBase64String(FileWriter.Result), "application/zip");
    }
  }
}