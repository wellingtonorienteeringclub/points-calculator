﻿namespace ONZ.PointsCalculator {
  public class Runner {
    public decimal Score { get; set; }
    public string Name { get; set; }
    public string Club { get; set; }
    public string Grade { get; set; }
    public int? Place { get; set; }
    public decimal SortValue { get; set; }
    public decimal OriginalScore { get; set; }
    public string OriginalGrade { get; set; }
    public IScoreCalculator ScoreCalculator { get; set; }
  }
}