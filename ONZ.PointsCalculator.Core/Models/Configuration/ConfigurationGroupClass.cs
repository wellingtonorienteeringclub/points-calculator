﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class ConfigurationGroupClass {
    public ConfigurationGroup ConfigurationGroup { get; set; }
    public string Name { get; set; }
    public int? RequiredNumbers { get; set; }
    public string AppendToBottomOf { get; set; }
    public List<int> Scores { get; set; }
    public int? MaxScore { get; set; }
  }
}