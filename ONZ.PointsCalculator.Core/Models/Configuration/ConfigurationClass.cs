﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class ConfigurationClass {
    public string Name { get; set; }
    public string ScoreType { get; set; }
    public List<int> Scores { get; set; }
    public int? MaxScore { get; set; }
    public string GroupName { get; set; }
    public string AppendToBottomOf { get; set; }
    public int? RequiredNumbers { get; set; }
  }
}