﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class Configuration : IConfiguration {
    public List<ConfigurationEvent> Events { get; set; }
    public List<ConfigurationTotalGroup> TotalGroups { get; set; }
    public bool? OutputConsole { get; set; }
    public bool? OutputHtml { get; set; }
    public bool? OutputDebug { get; set; }
    public bool? Clear { get; set; }
    public bool? Individual { get; set; }
    public int? ResultsToCount { get; set; }
    public bool? OutputClass { get; set; }
    public bool? OutputSummary { get; set; }
    public bool? SortDescending { get; set; }
    public string FilePrefix { get; set; }
    public string ClubHeading { get; set; }

    public List<ConfigurationEventTotalGroup> DefaultTotalGroups { get; set; }
    public string DefaultScoreType { get; set; }
    public int? DefaultRunnersToCount { get; set; }
    public List<int> DefaultScores { get; set; }
    public int? DefaultMaxScore { get; set; }
  }
}