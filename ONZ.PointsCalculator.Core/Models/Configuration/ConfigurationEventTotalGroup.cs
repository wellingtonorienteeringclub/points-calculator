﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class ConfigurationEventTotalGroup : ITotalGroup {
    public ConfigurationEvent ConfigurationEvent { get; set; }
    public string Title { get; set; }
    public string FileName { get; set; }
    public List<string> Groups { get; set; }
    public List<string> OnlyShow { get; set; }
  }
}