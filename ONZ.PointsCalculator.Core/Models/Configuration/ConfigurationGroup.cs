﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class ConfigurationGroup {
    public ConfigurationEvent ConfigurationEvent { get; set; }
    public string Name { get; set; }
    public List<ConfigurationGroupClass> Classes { get; set; }

    public string ScoreType { get; set; }
    public int? RunnersToCount { get; set; }

    public List<int> DefaultScores { get; set; }
    public int? DefaultMaxScore { get; set; }
  }
}