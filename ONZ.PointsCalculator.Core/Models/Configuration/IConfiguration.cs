﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public interface IConfiguration {
    bool? Clear { get; set; }
    string ClubHeading { get; set; }
    List<ConfigurationEvent> Events { get; set; }
    string FilePrefix { get; set; }
    bool? Individual { get; set; }
    bool? OutputClass { get; set; }
    bool? OutputConsole { get; set; }
    bool? OutputDebug { get; set; }
    bool? OutputHtml { get; set; }
    bool? OutputSummary { get; set; }
    int? ResultsToCount { get; set; }
    List<ConfigurationTotalGroup> TotalGroups { get; set; }
    bool? SortDescending { get; set; }
  }
}