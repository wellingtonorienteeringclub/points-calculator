﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public interface ITotalGroup {
    string FileName { get; set; }
    List<string> Groups { get; set; }
    List<string> OnlyShow { get; set; }
    string Title { get; set; }
  }
}