﻿namespace ONZ.PointsCalculator {
  public class ControlPointValue {
    public int ControlNumberStart { get; set; }
    public int ControlNumberEnd { get; set; }
    public int Points { get; set; }
  }
}