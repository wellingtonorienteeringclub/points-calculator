﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class ConfigurationEvent {
    public Configuration Configuration { get; set; }
    public string File { get; set; }
    public string OutFolder { get; set; }
    public List<ConfigurationGroup> Groups { get; set; }
    public List<ConfigurationEventTotalGroup> TotalGroups { get; set; }
    public List<string> Include { get; set; }
    public List<string> Ignore { get; set; }
    public bool? OutputSummary { get; set; }
    public bool? OutputClass { get; set; }
    public List<ControlPointValue> ControlPointValues { get; set; }

    public string DefaultScoreType { get; set; }
    public int? DefaultRunnersToCount { get; set; }
    public List<int> DefaultScores { get; set; }
    public int? DefaultMaxScore { get; set; }
  }
}