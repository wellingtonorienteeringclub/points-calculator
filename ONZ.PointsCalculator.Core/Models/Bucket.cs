﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class Bucket {
    public List<Runner> Runners { get; set; }
    public List<string> Mandatory { get; set; }
  }
}