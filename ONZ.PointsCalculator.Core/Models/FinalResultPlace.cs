﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class FinalResultPlace {
    public string Club { get; set; }
    public decimal Score { get; set; }
    public List<Runner> Runners { get; set; }
    public string Group { get; set; }
  }
}