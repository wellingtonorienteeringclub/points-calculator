﻿namespace ONZ.PointsCalculator {
  public class ClassResultGroup {
    public ResultClass Result { get; set; }
    public ConfigurationGroupClass Configuration { get; set; }
  }
}