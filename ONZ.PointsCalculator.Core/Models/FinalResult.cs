﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class FinalResult {
    public string Name { get; set; }
    public List<FinalResultPlace> Places { get; set; }
    public string EventName { get; set; }
  }
}