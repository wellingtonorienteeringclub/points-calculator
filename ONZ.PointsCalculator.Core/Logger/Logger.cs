﻿using System;
using System.Text;

namespace ONZ.PointsCalculator {
  public class Logger : ILogger {
    private StringBuilder _LogMessages = new StringBuilder();
    private StringBuilder _OutMessages = new StringBuilder();
    private bool _OutputConsole = true;

    public Logger(bool outputConsole) {
      _OutputConsole = outputConsole;
    }

    public void Log(string message) {
      _LogMessages.AppendLine(message);
      if (_OutputConsole) {
        Console.WriteLine(message);
      }
    }

    public void Out(string message) {
      _OutMessages.AppendLine(message);
      Log(message);
    }

    public string GetOutMessage() {
      return _OutMessages.ToString();
    }
  }
}