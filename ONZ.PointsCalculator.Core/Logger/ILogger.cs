﻿namespace ONZ.PointsCalculator {
  public interface ILogger {
    void Log(string message);
  }
}