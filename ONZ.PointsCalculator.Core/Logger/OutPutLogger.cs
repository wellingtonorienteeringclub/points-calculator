﻿using System;

namespace ONZ.PointsCalculator {
  public class OutPutLogger : ILogger {
    public void Log(string message) {
      Console.WriteLine(message);
    }
  }
}