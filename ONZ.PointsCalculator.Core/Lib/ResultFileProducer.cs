﻿using System.Collections.Generic;
using System.Linq;

namespace ONZ.PointsCalculator {
  public class ResultFileProducer {
    private readonly IResultFileProducer _ResultFileProducer;
    private readonly IConfiguration _Configurations;

    public ResultFileProducer(IResultFileProducer resultFileProducer, IConfiguration configurations) {
      _ResultFileProducer = resultFileProducer;
      _Configurations = configurations;
    }

    public void Produce(List<CalculateResult> allResults) {
      _ResultFileProducer.Init();

      foreach (var item in allResults) {
        var tg = item.Configuration.TotalGroups ?? item.Configuration.Configuration.DefaultTotalGroups;
        OutputResults(item.Results, item.Configuration.OutFolder, tg.OfType<ITotalGroup>().ToList(), null, item.Configuration.OutputSummary ?? true, item.Configuration.OutputClass ?? true);
      }

      OutputResults(allResults.SelectMany(p => p.Results).ToList(), "Total", _Configurations.TotalGroups.OfType<ITotalGroup>().ToList(), _Configurations.Events.Select(p => p.OutFolder).ToList(), _Configurations.OutputSummary ?? true, _Configurations.OutputClass ?? true);

      _ResultFileProducer.End();
    }

    private void OutputResults(List<FinalResult> finalResults, string type, List<ITotalGroup> totalGroups, List<string> events, bool writeSummaryResults, bool writeClassResults) {
      var master = events != null && events.Count > 0;

      var writers = new List<IResultWriter> { };

      if (!_Configurations.OutputHtml.HasValue || _Configurations.OutputHtml.Value) {
        writers.Add(new HtmlResultWriter());
      }

      foreach (var writer in writers) {
        var lookup = new Dictionary<ITotalGroup, string>();

        if (writeSummaryResults) {
          _ResultFileProducer.Write(type, $"{_Configurations.FilePrefix}results-full.{writer.FileEnding}", writer.Format(writer.PrintDetailedResults(finalResults, true, true, _Configurations)));
          _ResultFileProducer.Write(type, $"{_Configurations.FilePrefix}results-summary.{writer.FileEnding}", writer.Format(writer.PrintDetailedResults(finalResults, false, true, _Configurations)));
        }

        if (totalGroups != null) {
          foreach (var totalGroup in totalGroups) {
            var content = writer.PrintTotalResults(type + " - " + totalGroup.Title, totalGroup.Groups, finalResults, events, totalGroup.OnlyShow, false, _Configurations);
            if (!lookup.ContainsKey(totalGroup)) {
              lookup.Add(totalGroup, content);
            }

            if (writeClassResults) {
              _ResultFileProducer.Write(type, $"{_Configurations.FilePrefix}{totalGroup.FileName}.{writer.FileEnding}", writer.Format(writer.FileHeader + content + writer.FileFooter));
            }
          }
        }

        var masterWriter = writer as IMasterWriter;

        if (master && masterWriter != null) {
          var content = writer.Format(masterWriter.PrintMasterResults(lookup));
          _ResultFileProducer.Write(type, $"{_Configurations.FilePrefix}master.{writer.FileEnding}", content);
        }
      }
    }
  }
}