﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class GroupResultGroup {
    public string Name { get; set; }
    public List<ClassResultGroup> Classes { get; set; }
  }
}