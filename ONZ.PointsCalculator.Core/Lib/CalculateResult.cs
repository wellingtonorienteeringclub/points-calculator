﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class CalculateResult {
    public List<FinalResult> Results { get;  set; }
    public ConfigurationEvent Configuration { get;  set; }
  }
}