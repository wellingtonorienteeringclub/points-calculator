﻿namespace ONZ.PointsCalculator {
  public interface IJsonSerializerService {
    string Serialize<T>(T instance);
    T Deserialize<T>(string json);
  }
}