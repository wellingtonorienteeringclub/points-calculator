﻿using System;
using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class PointsScoreCalculator : IScoreCalculator {
    private readonly ConfigurationGroupClass _ConfigurationClass;
    private readonly List<int> _Scores;

    public PointsScoreCalculator(ConfigurationGroupClass configurationClass) {
      _ConfigurationClass = configurationClass;

      if (_ConfigurationClass.Scores != null && _ConfigurationClass.Scores.Count > 0) {
        _Scores = _ConfigurationClass.Scores;
      } else if (_ConfigurationClass.ConfigurationGroup.DefaultScores != null && _ConfigurationClass.ConfigurationGroup.DefaultScores.Count > 0) {
        _Scores = _ConfigurationClass.ConfigurationGroup.DefaultScores;
      } else if (_ConfigurationClass.ConfigurationGroup.ConfigurationEvent.DefaultScores != null && _ConfigurationClass.ConfigurationGroup.ConfigurationEvent.DefaultScores.Count > 0) {
        _Scores = _ConfigurationClass.ConfigurationGroup.ConfigurationEvent.DefaultScores;
      } else if (_ConfigurationClass.ConfigurationGroup.ConfigurationEvent.Configuration.DefaultScores != null && _ConfigurationClass.ConfigurationGroup.ConfigurationEvent.Configuration.DefaultScores.Count > 0) {
        _Scores = _ConfigurationClass.ConfigurationGroup.ConfigurationEvent.Configuration.DefaultScores;
      } else {
        throw new Exception("Please enter scores when score type is 'points'.");
      }
    }

    public void Adjust(Runner runner) {
      if (runner.Place.HasValue) {
        runner.Place -= 1;
        runner.SortValue = GetSortValue(runner.Place);
        runner.Score = GetScore(runner.Place);
      }
    }

    public decimal GetScore(ResultItem resultItem, List<ResultItem> allResults) {
      return GetScore(resultItem.Place);
    }

    public decimal GetScore(int? place) {
      if (!place.HasValue || place.Value < 1 || place.Value > _Scores.Count) {
        return decimal.MinValue;
      }

      return _Scores[place.Value - 1];
    }

    public decimal GetSortValue(ResultItem resultItem, List<ResultItem> allResults) {
      return GetSortValue(resultItem.Place);
    }

    public decimal GetSortValue(int? place) {
      return GetScore(place) + (1 / Convert.ToDecimal(_Scores[0]));
    }
  }
}