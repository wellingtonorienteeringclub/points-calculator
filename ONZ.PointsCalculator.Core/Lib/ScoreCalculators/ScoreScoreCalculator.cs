﻿using System.Collections.Generic;
using System.Linq;

namespace ONZ.PointsCalculator {
  public class ScoreScoreCalculator : IScoreCalculator {
    private ConfigurationEvent _Configuration;

    public ScoreScoreCalculator(ConfigurationGroupClass configuration) {
      _Configuration = configuration.ConfigurationGroup.ConfigurationEvent;
    }

    public void Adjust(Runner runner) {
      return;
    }

    public decimal GetScore(ResultItem resultItem, List<ResultItem> allResults) {
      if (resultItem.Controls == null) {
        return 0;
      }

      return resultItem.Controls.Select(p => {
        var v = _Configuration.ControlPointValues.FirstOrDefault(c => c.ControlNumberStart <= p && c.ControlNumberEnd >= p);

        return v == null ? 0 : v.Points;
      }).Sum();
    }

    public decimal GetSortValue(ResultItem resultItem, List<ResultItem> allResults) {
      return resultItem.Time ?? decimal.MaxValue;
    }
  }
}