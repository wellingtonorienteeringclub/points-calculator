﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public interface IScoreCalculator {
    decimal GetScore(ResultItem resultItem, List<ResultItem> allResults);
    decimal GetSortValue(ResultItem resultItem, List<ResultItem> allResults);
    void Adjust(Runner runner);
  }
}