﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ONZ.PointsCalculator {
  public class PercentScoreCalculator : IScoreCalculator {
    private readonly ConfigurationGroupClass _ConfigurationClass;

    public PercentScoreCalculator(ConfigurationGroupClass configurationClass) {
      _ConfigurationClass = configurationClass;

      if (!configurationClass.MaxScore.HasValue) {
        throw new Exception("Please enter a max score when score type is 'step'.");
      }
    }

    public void Adjust(Runner runner) {
      return;
    }

    public decimal GetScore(ResultItem resultItem, List<ResultItem> allResults) {
      var bestScore = allResults.Where(p => p.Time.HasValue).Min(p => p.Time);

      if (!bestScore.HasValue) {
        throw new Exception("Could not determine lowest time score when score type was 'percent'.");
      }

      if (!resultItem.Place.HasValue) {
        return decimal.MinValue;
      }

      return resultItem.Time.HasValue ? Math.Round(bestScore.Value / resultItem.Time.Value * 100, 1) : decimal.MinValue;
    }

    public decimal GetSortValue(ResultItem resultItem, List<ResultItem> allResults) {
      return resultItem.Place ?? 0;
    }
  }
}