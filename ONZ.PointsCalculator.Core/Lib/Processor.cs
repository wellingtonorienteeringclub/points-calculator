﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ONZ.PointsCalculator {
  public class Processor {
    private readonly ILogger _OutputLogger;
    private readonly IResultLoader _ResultLoader;
    private readonly IResultFileLoader _ResultFileLoader;
    private IConfiguration _Configurations;

    public Processor(ILogger outputLogger, IResultLoader resultLoader, IResultFileLoader resultFileLoader, IConfiguration configurations) {
      _OutputLogger = outputLogger;
      _ResultLoader = resultLoader;
      _ResultFileLoader = resultFileLoader;
      _Configurations = configurations;
    }

    public List<CalculateResult> Process() {
      _OutputLogger.Log("Loading configuration.");

      var allResults = new List<CalculateResult>();

      foreach (var configuration in _Configurations.Events) {
        var finalResults = ProcessEvent(configuration);

        foreach (var finalResult in finalResults) {
          if (!_Configurations.SortDescending.HasValue || _Configurations.SortDescending.Value) {
            finalResult.Places = finalResult.Places.OrderByDescending(p => p.Score).ToList();
          } else {
            finalResult.Places = finalResult.Places.OrderBy(p => p.Score).ToList();
          }
        }

        allResults.Add(new CalculateResult { Results = finalResults, Configuration = configuration });
      }

      return allResults;
    }

    private List<FinalResult> ProcessEvent(ConfigurationEvent configuration) {
      if (String.IsNullOrWhiteSpace(configuration.File)) {
        return new List<FinalResult>();
      }

      byte[] rawFile = _ResultFileLoader.Load(configuration.File);

      if (rawFile == null) {
        _OutputLogger.Log($"No file content for file {configuration.File}.");
        return new List<FinalResult>();
      }

      _OutputLogger.Log("Loading results.");
      var results = _ResultLoader.Load(rawFile);

      var responses = new List<FinalResult>();

      foreach (var group in configuration.Groups) {
        var classResultGroups = new List<ClassResultGroup>();

        foreach (var i in group.Classes) {
          classResultGroups.Add(new ClassResultGroup {
            Configuration = i,
            Result = results.FirstOrDefault(p => p.Name == i.Name)
          });
        }

        responses.Add(CalculatedResult(group.Name, classResultGroups, configuration, group));
      }

      return responses;
    }

    private FinalResult CalculatedResult(string key, List<ClassResultGroup> results, ConfigurationEvent configuration, ConfigurationGroup groupConfiguration) {
      _OutputLogger.Log($"Processing {key}.");
      _OutputLogger.Log($"Merging {key}...");

      List<Runner> runners = PrepareRunners(results, configuration);

      var buckets = new Dictionary<string, Bucket>();

      _OutputLogger.Log($"Assigning to buckets...");

      _OutputLogger.Log($"Sorting {key}...");
      runners = runners.OrderByDescending(p => p.SortValue).ToList();

      int maxNumber = groupConfiguration.RunnersToCount ?? groupConfiguration.ConfigurationEvent.DefaultRunnersToCount ?? groupConfiguration.ConfigurationEvent.Configuration.DefaultRunnersToCount ?? Int32.MaxValue;

      List<string> mandatory = new List<string>();

      foreach (var c in groupConfiguration.Classes) {
        if (c.RequiredNumbers.HasValue && c.RequiredNumbers.Value > 0) {
          for (var i = 0; i < c.RequiredNumbers.Value; i++) {
            mandatory.Add(c.Name);
          }
        }
      }

      bool individual = (!_Configurations.Individual.HasValue || _Configurations.Individual.Value);

      while (runners.Count > 0) {
        var r = runners[0];

        string bucketKey = individual ? $"{r.Name} - {r.Club}" : r.Club;

        if (!buckets.ContainsKey(bucketKey)) {
          buckets.Add(bucketKey, new Bucket {
            Mandatory = mandatory.Select(p => p).ToList(), //Deep copy
            Runners = new List<Runner>()
          });
        }

        var bucket = buckets[bucketKey];

        bool ignoreClub = configuration.Ignore != null && configuration.Ignore.Any(i => r.Club.ToLower().Contains(i.ToLower())) || configuration.Include != null && configuration.Include.Count > 0 && !configuration.Include.Any(i => r.Club.ToLower().Contains(i.ToLower()));
        bool bucketFull = bucket.Runners.Count >= maxNumber;
        bool notMissingRunner = false;

        if (bucket.Mandatory.Count > 0) {
          int index = bucket.Mandatory.IndexOf(r.Grade);
          if (index < 0) {
            _OutputLogger.Log($"Runner is not running in a mandatory grade {r.Grade}, mandatory grades are {String.Join(",", bucket.Mandatory)}.");
            notMissingRunner = maxNumber - bucket.Runners.Count - bucket.Mandatory.Count < 1;
          }
        }

        if (ignoreClub || bucketFull || notMissingRunner) {
          if (ignoreClub) {
            _OutputLogger.Log($"{r.Club} is a club that should be ignored. Discarding {r.Name}.");
          } else if (bucketFull) {
            _OutputLogger.Log($"{r.Club} already have {maxNumber} runners. Discarding {r.Name}.");
          } else if (notMissingRunner) {
            _OutputLogger.Log($"{r.Club} already have maximum runners in that grade. Discarding {r.Name}.");
          }

          _OutputLogger.Log($"Promoting {r.Grade.ToLower()} runners.");

          runners.RemoveAt(0);

          runners.Where(p => p.Grade == r.Grade && p.Place > r.Place).ToList().ForEach(runner => {
            runner.ScoreCalculator.Adjust(runner);
          });

          _OutputLogger.Log($"Sorting {key}...");
          runners = runners.OrderByDescending(p => p.SortValue).ToList();
          continue;
        } else {
          int index = bucket.Mandatory.IndexOf(r.Grade);
          if (index > -1) {
            bucket.Mandatory.RemoveAt(index);
            _OutputLogger.Log($"Removing mandatory runner for {r.Club} in grade {r.Grade}.");
          }

          string scoreMessage = "";

          if (r.Score != decimal.MinValue) {
            scoreMessage = $" with {r.Score}";
          }

          _OutputLogger.Log($"Assigning {r.Name} to {r.Club} bucket{scoreMessage}.");
          bucket.Runners.Add(r);

          runners.RemoveAt(0);
        }
      }

      _OutputLogger.Log($"Removing runner scoring less or 0 points.");
      runners = buckets.Select(p => p.Value).SelectMany(p => p.Runners).Where(p => p.Score > 0).ToList();

      var finalOrder = runners.GroupBy(p => individual ? $"{p.Name} - {p.Club}" : p.Club).Select(p => new FinalResultPlace { Group = key, Club = p.Select(r => r.Club).FirstOrDefault(), Score = p.Sum(r => r.Score), Runners = p.ToList() }).OrderByDescending(p => p.Score).ToList();

      return new FinalResult {
        Name = key,
        Places = finalOrder,
        EventName = configuration.OutFolder
      };
    }

    private List<Runner> PrepareRunners(List<ClassResultGroup> group, ConfigurationEvent configurationEvent) {
      group = group.Where(p => p.Result != null).ToList();

      var runners = new List<Runner>();

      var joinGroups = group.Where(g => !String.IsNullOrWhiteSpace(g.Configuration.AppendToBottomOf)).ToList();
      joinGroups.Reverse();

      foreach (var g in joinGroups) {
        var target = group.FirstOrDefault(p => p.Configuration.Name == g.Configuration.AppendToBottomOf);

        if (target == null) {
          continue;
        }

        int? lastPlace = target.Result.Placings.Where(p => p.Place.HasValue).Max(p => p.Place);

        if (!lastPlace.HasValue) {
          continue;
        }

        foreach (var placing in g.Result.Placings) {
          target.Result.Placings.Add(new ResultItem {
            Club = placing.Club,
            Name = placing.Name,
            Place = placing.Place.HasValue ? placing.Place.Value + lastPlace : null,
            Grade = placing.Grade
          });
        }
      }

      foreach (var g in group.Where(g => String.IsNullOrWhiteSpace(g.Configuration.AppendToBottomOf))) {
        var scoreCalculator = GetScoreCalculator(g.Configuration);

        foreach (var r in g.Result.Placings) {
          var score = scoreCalculator.GetScore(r, g.Result.Placings);
          var grade = g.Result.Name;

          runners.Add(
            new Runner {
              Club = r.Club,
              Grade = grade,
              OriginalGrade = r.Grade,
              Name = r.Name,
              Score = score,
              OriginalScore = score,
              SortValue = scoreCalculator.GetSortValue(r, g.Result.Placings),
              Place = r.Place,
              ScoreCalculator = scoreCalculator
            });
        }
      }

      return runners;
    }

    private IScoreCalculator GetScoreCalculator(ConfigurationGroupClass configurationClass) {
      string scoreType = null;

      if (!String.IsNullOrWhiteSpace(configurationClass.ConfigurationGroup.ScoreType)) {
        scoreType = configurationClass.ConfigurationGroup.ScoreType;
      } else if (!String.IsNullOrWhiteSpace(configurationClass.ConfigurationGroup.ConfigurationEvent.DefaultScoreType)) {
        scoreType = configurationClass.ConfigurationGroup.ConfigurationEvent.DefaultScoreType;
      } else if (!String.IsNullOrWhiteSpace(configurationClass.ConfigurationGroup.ConfigurationEvent.Configuration.DefaultScoreType)) {
        scoreType = configurationClass.ConfigurationGroup.ConfigurationEvent.Configuration.DefaultScoreType;
      }

      if (scoreType == "percent") {
        return new PercentScoreCalculator(configurationClass);
      }

      if (scoreType == "score") {
        return new ScoreScoreCalculator(configurationClass);
      }

      if (scoreType == "points") {
        return new PointsScoreCalculator(configurationClass);
      }

      throw new Exception("Please specify a score type.");
    }
  }
}