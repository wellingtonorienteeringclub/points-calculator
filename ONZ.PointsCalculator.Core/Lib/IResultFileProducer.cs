﻿namespace ONZ.PointsCalculator {
  public interface IResultFileProducer {
    void Write(string competition, string fileName, string message);
    void WriteInstance<T>(string competition, string fileName, T instance);
    void Init();
    void End();
  }
}