﻿namespace ONZ.PointsCalculator {
  public interface IResultFileLoader {
    byte[] Load(string file);
  }
}