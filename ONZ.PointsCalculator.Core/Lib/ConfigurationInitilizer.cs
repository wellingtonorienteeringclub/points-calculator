﻿namespace ONZ.PointsCalculator {
  public class ConfigurationInitilizer {
    public static IConfiguration Initilize(Configuration config) {

      foreach (var e in config.Events) {
        e.Configuration = config;
        foreach (var g in e.Groups) {
          g.ConfigurationEvent = e;
          foreach (var c in g.Classes) {
            c.ConfigurationGroup = g;
          }
        }
        if (e.TotalGroups != null) {
          foreach (var g in e.TotalGroups) {
            g.ConfigurationEvent = e;
          }
        }
      }

      foreach (var g in config.TotalGroups) {
        g.Configuration = config;
      }

      return config;
    }
  }
}