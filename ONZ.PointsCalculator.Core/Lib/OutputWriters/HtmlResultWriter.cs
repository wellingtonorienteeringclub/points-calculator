﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ONZ.PointsCalculator {
  public class HtmlResultWriter : IResultWriter, IMasterWriter {
    public string FileEnding => "html";

    public string FileHeader => Constants.HtmlStart;

    public string FileFooter => Constants.HtmlEnd;

    public string PrintTotalResults(string type, List<string> groups, List<FinalResult> finalResults, List<string> events, List<string> onlyShow, bool includeHeaders, IConfiguration configuration) {
      bool individual = !configuration.Individual.HasValue || configuration.Individual.Value;
      string clubHeading = String.IsNullOrWhiteSpace(configuration.ClubHeading) ? "Club" : configuration.ClubHeading;
      bool sortDescending = !configuration.SortDescending.HasValue || configuration.SortDescending.Value;

      StringBuilder output = new StringBuilder();

      var gs = finalResults
        .Where(p => groups.Contains(p.Name))
        .SelectMany(p => p.Places, (p, c) => new { EventName = p.EventName, Item = c })
        .GroupBy(p => individual ? $"{p.Item.Runners[0].Name} - {p.Item.Club}" : p.Item.Club);

      try {
        var l = gs
          .Select(p => new {
            Club = p.Key,
            Total = p.Sum(p2 => p2.Item.Score),
            LookUp = p.ToDictionary(k2 => events == null ? k2.Item.Group : k2.EventName + " - " + k2.Item.Group, v2 => v2.Item.Score)
          });

        var lookups = sortDescending ? l.OrderByDescending(p => p.Total).ToList() : l.OrderBy(p => p.Total).ToList();

        var headings = new List<string>();

        if (events != null) {
          foreach (var e in events) {
            foreach (var g in groups) {
              if (!headings.Contains(g)) {
                headings.Add(g);
              }
            }
          }
        } else {
          headings = groups;
        }

        if (includeHeaders) {
          output.Append(Constants.HtmlStart);
        }

        output.AppendLine($"<h2>{type}</h2>");
        output.AppendLine($"<table class=\"table\">");
        output.AppendLine($"<tr><th>Place</th><th>{clubHeading}</th><th>Total</th><th>{String.Join("</th><th>", headings)}</th></tr>");

        int place = 0;
        decimal previousScore = -1;
        int same = 0;

        foreach (var lookup in lookups) {
          if (previousScore != lookup.Total) {
            place = place + 1 + same;
            same = 0;
          } else {
            same++;
          }

          List<string> groupsBuilder = new List<string>();

          foreach (var g in headings) {
            if (lookup.LookUp.ContainsKey(g)) {
              groupsBuilder.Add(lookup.LookUp[g].ToString());
            } else {
              groupsBuilder.Add("");
            }
          }

          if (onlyShow == null || onlyShow.Count < 1 || onlyShow.Contains(lookup.Club)) {
            if (events == null) {
              output.AppendLine($"<tr><td>{place}</td><td>{lookup.Club}</td><td>{lookup.Total}</td><th>{String.Join("</th><th>", groupsBuilder)}</th></tr>");
            } else {
              output.AppendLine($@"<tr><th rowspan=""{events.Count + 1}"">{place}</th><th>{lookup.Club}</th><th>{lookup.Total}</th><th colspan=""{headings.Count}""></th></tr>");

              foreach (var e in events) {
                List<string> eBuilder = new List<string>();
                decimal rowTotal = 0;

                foreach (var g in headings) {
                  string key = e + " - " + g;
                  if (lookup.LookUp.ContainsKey(key)) {
                    var points = lookup.LookUp[key];
                    eBuilder.Add(points.ToString());
                    rowTotal += points;
                  } else {
                    eBuilder.Add("-");
                  }
                }

                output.AppendLine($@"<tr><td class=""text-right"">{e}:</td><td>{rowTotal}</td><td>{String.Join("</td><td>", eBuilder)}</td></tr>");
              }
            }
          }

          previousScore = lookup.Total;
        }

        output.Append("</table>");

        if (includeHeaders) {
          output.Append(Constants.HtmlEnd);
        }
      } catch (Exception ex) {
        string test = ex.ToString();
      }

      return output.ToString();
    }

    public string PrintDetailedResults(List<FinalResult> finalResults, bool verbose, bool includeHeaders, IConfiguration configuration) {
      string clubHeading = String.IsNullOrWhiteSpace(configuration.ClubHeading) ? "Club" : configuration.ClubHeading;

      StringBuilder output = new StringBuilder();

      if (includeHeaders) {
        output.Append(Constants.HtmlStart);
      }

      foreach (var finalResult in finalResults) {

        output.AppendLine($"<h2>{finalResult.EventName} - {finalResult.Name}</h2>");
        output.Append("<table class=\"table\">");

        if (!verbose) {
          output.AppendLine($"<tr><th>Place</th><th>{clubHeading}</th><th>Points</th></tr>");
        } else {
          output.AppendLine($"<tr><th>Place</th><th>{clubHeading}</th><th>Points</th><th>Grade</th><th>Individual Points</th><th>Original Points</th></tr>");
        }

        int place = 0;
        decimal previousScore = -1;
        int same = 0;

        foreach (var result in finalResult.Places) {
          if (previousScore != result.Score) {
            place = place + 1 + same;
            same = 0;
          } else {
            same++;
          }

          output.AppendLine($"<tr><td>{place}</td><td>{result.Club}</td><td colspan=\"4\">{result.Score}</td></tr>");

          if (verbose) {
            foreach (var r in result.Runners) {
              var originalPoints = r.OriginalScore < 0 ? "0" : $"{r.OriginalScore}";
              output.AppendLine($"<tr><td colspan=\"2\"></td><td>{r.Name}</td><td>{r.OriginalGrade}</td><td>{r.Score}</td><td>{originalPoints}</td></tr>");
            }
          }

          previousScore = result.Score;
        }

        output.Append("</table>");
      }

      if (includeHeaders) {
        output.Append(Constants.HtmlEnd);
      }

      return output.ToString();
    }

    public string PrintMasterResults(Dictionary<ITotalGroup, string> lookup) {
      StringBuilder output = new StringBuilder();
      output.Append(Constants.HtmlStart);
      output.AppendLine(@"<div class=""row"">");
      output.AppendLine(@"<div class=""col-lg-2"">");
      output.AppendLine(@"<ul class=""nav nav-pills nav-stacked affix"">");
      foreach (var item in lookup) {
        output.AppendLine($@"<li class=""""><a href=""#{item.Key.FileName}"">{item.Key.Title}</a></li>");
      }
      output.AppendLine(@"</ul>");
      output.AppendLine(@"</div>");
      output.AppendLine(@"<div class=""col-lg-10"">");

      foreach (var item in lookup) {
        output.AppendLine($@"<div id=""{item.Key.FileName}"">");
        output.AppendLine(item.Value);
        output.AppendLine(@"</div>");
      }

      output.AppendLine(@"</div>");
      output.AppendLine(@"</div>");
      output.Append(Constants.HtmlEnd);
      return output.ToString();
    }

    public string Format(string input) {
      try {
        return XElement.Parse(input).ToString();
      } catch {
        return input;
      }
    }
  }
}