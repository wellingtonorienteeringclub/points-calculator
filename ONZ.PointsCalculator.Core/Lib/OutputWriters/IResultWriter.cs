﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public interface IResultWriter {
    string FileEnding { get; }
    string FileHeader { get; }
    string FileFooter { get; }

    string PrintDetailedResults(List<FinalResult> finalResults, bool verbose, bool includeHeaders, IConfiguration configuration);
    string PrintTotalResults(string type, List<string> groups, List<FinalResult> finalResults, List<string> events, List<string> onlyShow, bool includeHeaders, IConfiguration configuration);
    string Format(string input);
  }
}