﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  internal interface IMasterWriter {
    string PrintMasterResults(Dictionary<ITotalGroup, string> lookup);
  }
}