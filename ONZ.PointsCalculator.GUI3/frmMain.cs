﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using ONZ.MapFramer.GUI;

namespace ONZ.PointsCalculator.GUI3 {
  public partial class FrmMain : Form {

    public string _FilePath = null;

    public FrmMain() {
      InitializeComponent();
    }

    private void btnSelect_Click(object sender, EventArgs e) {
      SelectFile();
    }

    private void btnRun_Click(object sender, EventArgs e) {
      Run();
    }

    private void btnExit_Click(object sender, EventArgs e) {
      Exit();
    }

    private void Run() {
      txtLog.Text = "";
      var bl = new BufferLogger();

      _FilePath = txtFilePath.Text;

      if (String.IsNullOrWhiteSpace(_FilePath) || !File.Exists(_FilePath)) {
        MessageBox.Show("Please select a valid configuration file.", "No configuration file", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      IConfiguration c = null;
      IJsonSerializerService jss = new JsonSerializerService();

      try {
        var json = File.ReadAllText(_FilePath);
        var config = jss.Deserialize<Configuration>(json);
        c = ConfigurationInitilizer.Initilize(config);
      } catch (Exception ex) {
        MessageBox.Show("Please select a valid configuration file.", "No configuration file", MessageBoxButtons.OK, MessageBoxIcon.Error);
        bl.Log(ex.ToString());
        txtLog.Text = bl.End();
        return;
      }

      if (c == null) {
        MessageBox.Show("Please select a valid configuration file.", "No configuration file", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      var basePath = Path.GetDirectoryName(_FilePath);
      IResultLoader rl = new IOFXMLResultLoader();
      IResultFileLoader rfl = new FileSystemResultFileLoader(basePath);
      IResultFileProducer fw = new FileWriter(Path.Combine(basePath, "out"), c, jss);

      var result = new Processor(bl, rl, rfl, c).Process();
      new ResultFileProducer(fw, c).Produce(result);

      txtLog.Text = bl.End();

      MessageBox.Show("All done!", "Done!", MessageBoxButtons.OK, MessageBoxIcon.Information);

      if (cbOpen.Checked) {
        Process.Start("explorer.exe", Path.Combine(basePath, "out"));
      }
    }

    private void Exit() {
      Application.Exit();
    }

    private void SelectFile() {
      openFileDialog1.DefaultExt = "json";
      openFileDialog1.FileName = "points.json";

      if (openFileDialog1.ShowDialog() == DialogResult.OK) {
        txtFilePath.Text = openFileDialog1.FileName;
      }
    }
  }
}
