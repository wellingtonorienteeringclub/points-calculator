﻿using System.IO;

namespace ONZ.PointsCalculator.GUI3 {
  public class FileSystemResultFileLoader : IResultFileLoader {
    private string _BasePath;

    public FileSystemResultFileLoader(string basePath) {
      _BasePath = basePath;
    }

    public byte[] Load(string file) {
      string path = Path.Combine(_BasePath, file);

      if (!File.Exists(path)) {
        return null;
      }

      return File.ReadAllBytes(path);
    }
  }
}
