﻿namespace ONZ.PointsCalculator.GUI3 {
  partial class FrmMain {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.btnRun = new System.Windows.Forms.Button();
      this.btnExit = new System.Windows.Forms.Button();
      this.txtLog = new System.Windows.Forms.TextBox();
      this.txtFilePath = new System.Windows.Forms.TextBox();
      this.btnSelect = new System.Windows.Forms.Button();
      this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
      this.cbOpen = new System.Windows.Forms.CheckBox();
      this.SuspendLayout();
      // 
      // btnRun
      // 
      this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
      this.btnRun.Location = new System.Drawing.Point(24, 73);
      this.btnRun.Margin = new System.Windows.Forms.Padding(6);
      this.btnRun.Name = "btnRun";
      this.btnRun.Size = new System.Drawing.Size(886, 44);
      this.btnRun.TabIndex = 3;
      this.btnRun.Text = "Calculate";
      this.btnRun.UseVisualStyleBackColor = true;
      this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
      // 
      // btnExit
      // 
      this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnExit.Location = new System.Drawing.Point(760, 435);
      this.btnExit.Margin = new System.Windows.Forms.Padding(6);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(150, 44);
      this.btnExit.TabIndex = 5;
      this.btnExit.Text = "Exit";
      this.btnExit.UseVisualStyleBackColor = true;
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // txtLog
      // 
      this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtLog.Location = new System.Drawing.Point(26, 131);
      this.txtLog.Margin = new System.Windows.Forms.Padding(6);
      this.txtLog.Multiline = true;
      this.txtLog.Name = "txtLog";
      this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txtLog.Size = new System.Drawing.Size(880, 289);
      this.txtLog.TabIndex = 4;
      // 
      // txtFilePath
      // 
      this.txtFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
      this.txtFilePath.BackColor = System.Drawing.SystemColors.ControlLightLight;
      this.txtFilePath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtFilePath.Location = new System.Drawing.Point(26, 21);
      this.txtFilePath.Margin = new System.Windows.Forms.Padding(6);
      this.txtFilePath.Name = "txtFilePath";
      this.txtFilePath.Size = new System.Drawing.Size(722, 31);
      this.txtFilePath.TabIndex = 1;
      // 
      // btnSelect
      // 
      this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSelect.Location = new System.Drawing.Point(760, 21);
      this.btnSelect.Margin = new System.Windows.Forms.Padding(6);
      this.btnSelect.Name = "btnSelect";
      this.btnSelect.Size = new System.Drawing.Size(150, 44);
      this.btnSelect.TabIndex = 2;
      this.btnSelect.Text = "Select Config";
      this.btnSelect.UseVisualStyleBackColor = true;
      this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
      // 
      // openFileDialog1
      // 
      this.openFileDialog1.FileName = "openFileDialog1";
      // 
      // cbOpen
      // 
      this.cbOpen.AutoSize = true;
      this.cbOpen.Checked = true;
      this.cbOpen.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbOpen.Location = new System.Drawing.Point(26, 444);
      this.cbOpen.Name = "cbOpen";
      this.cbOpen.Size = new System.Drawing.Size(335, 29);
      this.cbOpen.TabIndex = 6;
      this.cbOpen.Text = "Open Explorer after calculation";
      this.cbOpen.UseVisualStyleBackColor = true;
      // 
      // FrmMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(934, 502);
      this.Controls.Add(this.cbOpen);
      this.Controls.Add(this.btnSelect);
      this.Controls.Add(this.txtFilePath);
      this.Controls.Add(this.txtLog);
      this.Controls.Add(this.btnExit);
      this.Controls.Add(this.btnRun);
      this.Margin = new System.Windows.Forms.Padding(6);
      this.Name = "FrmMain";
      this.Text = "Points Calculator";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnRun;
    private System.Windows.Forms.Button btnExit;
    private System.Windows.Forms.TextBox txtLog;
    private System.Windows.Forms.TextBox txtFilePath;
    private System.Windows.Forms.Button btnSelect;
    private System.Windows.Forms.OpenFileDialog openFileDialog1;
    private System.Windows.Forms.CheckBox cbOpen;
  }
}

