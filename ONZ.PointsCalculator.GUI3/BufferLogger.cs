﻿using System.Text;
using ONZ.PointsCalculator;

namespace ONZ.MapFramer.GUI {
  internal class BufferLogger : ILogger {
    private StringBuilder _SB = new StringBuilder();

    public string End() {
      return _SB.ToString();
    }

    public void Log(string message) {
      _SB.AppendLine(message);
    }
  }
}