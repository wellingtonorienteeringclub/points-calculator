﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public interface IResultLoader {
    List<ResultClass> Load(byte[] rawFile);
  }
}