﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class ResultItem {
    public int? Place { get; set; }
    public string Name { get; set; }
    public string Club { get; set; }
    public string Grade { get; set; }
    public decimal? Time { get; set; }
    public List<int> Controls { get; set; }
  }
}