﻿using System.Collections.Generic;

namespace ONZ.PointsCalculator {
  public class ResultClass {
    public string Name { get; set; }
    public List<ResultItem> Placings { get; set; }
  }
}