﻿using System.IO;

namespace ONZ.PointsCalculator.Console {
  partial class Program {
    static void Main(string[] args) {
      IJsonSerializerService jss = new JsonSerializerService();

      var json = File.ReadAllText("config.json");
      var config = jss.Deserialize<Configuration>(json);
      var c = ConfigurationInitilizer.Initilize(config);
      var rl = new IOFXMLResultLoader();
      IResultFileLoader rfl = null;
      var fw = new FileWriter("./", c, jss);
      var result = new Processor(new OutPutLogger(), rl, rfl, c).Process();
      new ResultFileProducer(fw, c).Produce(result);
      System.Console.WriteLine("".PadRight(40, '*'));
      System.Console.WriteLine("Done!");
      System.Console.WriteLine("".PadRight(40, '*'));
      System.Console.WriteLine("Press any key to exit.");
      System.Console.ReadLine();
    }
  }
}