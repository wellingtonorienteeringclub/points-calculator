﻿using System.IO;

namespace ONZ.PointsCalculator.Console {
  public class FileWriter : IResultFileProducer {
    private readonly string _BasePath;
    private readonly IConfiguration _Configurations;
    private readonly IJsonSerializerService _JsonSerializerService;

    public FileWriter(string basePath, IConfiguration configurations, IJsonSerializerService jsonSerializerService) {
      _BasePath = basePath;
      _Configurations = configurations;
      _JsonSerializerService = jsonSerializerService;
    }

    public void End() {
      return;
    }

    public void Init() {
      Directory.CreateDirectory(Path.Combine(_BasePath, "out"));

      if (!_Configurations.Clear.HasValue || _Configurations.Clear.Value) {
        DirectoryInfo di = new DirectoryInfo(Path.Combine(_BasePath, "out"));

        foreach (FileInfo file in di.EnumerateFiles()) {
          file.Delete();
        }
        foreach (DirectoryInfo dir in di.EnumerateDirectories()) {
          dir.Delete(true);
        }
      }
    }

    public void Write(string folder, string fileName, string message) {
      if (!Directory.Exists(_BasePath)) {
        Directory.CreateDirectory(_BasePath);
      }

      var folderName = Path.Combine(_BasePath, folder.Replace(" ", "-"));

      if (!Directory.Exists(folderName)) {
        Directory.CreateDirectory(folderName);
      }

      string path = Path.Combine(folderName, fileName).Replace("/", "").Replace(" ", "-");

      File.WriteAllText(path, message);
    }

    public void WriteInstance<T>(string folder, string fileName, T instance) {
      Write(folder, fileName, _JsonSerializerService.Serialize(instance));
    }
  }
}